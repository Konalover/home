<?php

namespace App\Tests\Service;

use App\Service\MainService;
use PHPUnit\Framework\TestCase;

class MainServiceTest extends TestCase
{
    public function setUp(): void
    {
        $this->mainService = new MainService();
    }

    /**
     * @dataProvider parenthesesDataProvider
     * @param $expected
     * @param string $string
     * @return void
     */
    public function testIsCorrectParenthesis($expected, string $string): void
    {
        $result = $this->mainService->isCorrectParenthesis($string);

        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider multiplyArrayDataProvider
     * @param $expected
     * @param array $array
     * @param int $int
     * @return void
     */
    public function testMultiplyArrayValues($expected, array $array, int $int): void
    {
        $result = $this->mainService->multiplyArrayValues($array, $int);

        $this->assertEquals($expected, $result);
    }

    public function multiplyArrayDataProvider(): array
    {
        $array = [1, 2, [10, [100, 200], 20, 30], 3, [40, 50]];
        $int = 2;
        $result = [2, 4, [20, [200, 400], 40, 60], 6, [80, 100]];

        $array2 = [5, 1, [8], [10, 10], 6];
        $int2 = 3;
        $result2 = [15, 3, [24], [30, 30], 18];

        $array3 = [0, 6, [1, [6, [3,4]]]];
        $int3 = 2;
        $result3 = [0, 12, [2, [12, [6,8]]]];

        return [
            [$result, $array, $int],
            [$result2, $array2, $int2],
            [$result3, $array3, $int3]
        ];
    }

    public function parenthesesDataProvider(): array
    {
        $string = "mary (had a [little] lamb)";
        $string2 = "mary ([{had a [little] lamb)";
        $string3 = "mary (had a [little] ))lamb)";
        $string4 = "mary (had a [little] lamb {and} ((horse)))";

        return [
            [true, $string],
            [false, $string2],
            [false, $string3],
            [true, $string4]
        ];
    }

    /**
     * @dataProvider getNumbersDataProvider
     * @param $expected
     * @param array $array
     * @param int $int
     * @return void
     */
    public function testGetNumbersForCorrectSum($expected, array $array, int $int): void
    {
        $result = $this->mainService->getNumbersForCorrectSum($array, $int);

        $this->assertEquals($expected, $result);
    }

    public function getNumbersDataProvider(): array
    {
        $array  = [-3, 0, 2, 4, 5];
        $array2 = [-3, -1, 0, 2, 6];
        $array3 = [2, 4, 5];
        $array4 = [-2, -1, 1, 2];
        $array5 = [1, -6, 7, 3, 5];

        return [
            [[2,5], $array, 7],
            [[0,6], $array2, 6],
            [[], $array3, 8],
            [[-2,2, -1, 1], $array4, 0],
            [[1, -6], $array5, -5],
        ];
    }
}
