<?php
declare(strict_types=1);

namespace App\Service;

class MainService
{
    private const OPEN_ROUND_PARENTHESIS    = '(';
    private const OPEN_SQUARE_PARENTHESIS   = '[';
    private const OPEN_CURLY_PARENTHESIS    = '{';
    private const CLOSE_ROUND_PARENTHESIS   = ')';
    private const CLOSE_SQUARE_PARENTHESIS  = ']';
    private const CLOSE_CURLY_PARENTHESIS   = '}';

    private const OPEN_PARENTHESES = [
        self::OPEN_ROUND_PARENTHESIS,
        self::OPEN_SQUARE_PARENTHESIS,
        self::OPEN_CURLY_PARENTHESIS
    ];

    private const PARENTHESES = [
        self::OPEN_ROUND_PARENTHESIS,
        self::OPEN_SQUARE_PARENTHESIS,
        self::OPEN_CURLY_PARENTHESIS,
        self::CLOSE_ROUND_PARENTHESIS,
        self::CLOSE_SQUARE_PARENTHESIS,
        self::CLOSE_CURLY_PARENTHESIS
    ];

    private const PARENTHESES_PAIR = [
        self::OPEN_ROUND_PARENTHESIS  => self::CLOSE_ROUND_PARENTHESIS,
        self::OPEN_SQUARE_PARENTHESIS => self::CLOSE_SQUARE_PARENTHESIS,
        self::OPEN_CURLY_PARENTHESIS  => self::CLOSE_CURLY_PARENTHESIS
    ];

    /**
     * @param string $string
     * @return bool
     */
    public function isCorrectParenthesis(string $string): bool
    {
        $openParentheses = [];
        for ($i = 0, $iMax = strlen($string); $i < $iMax; $i++) {
            $symbol = $string[$i];
            if (!$this->isParenthesis($symbol)) {
                continue;
            }

            if ($this->isOpenParenthesis($symbol)) {
                $openParentheses[] = $symbol;
                continue;
            }

            $lastOpenParenthesis = array_pop($openParentheses);

            if ($this->hasExtraCloseParenthesis($symbol, $lastOpenParenthesis)) {
                return false;
            }

            if (!$this->isCorrectParenthesisPair($symbol, $lastOpenParenthesis)) {
                return false;
            }
        }

        if ($this->hasExtraOpenParenthesis($openParentheses)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $closeParenthesis
     * @param string $lastOpenParenthesis
     * @return bool
     */
    private function isCorrectParenthesisPair(string $closeParenthesis, string $lastOpenParenthesis): bool
    {
        return self::PARENTHESES_PAIR[$lastOpenParenthesis] === $closeParenthesis;
    }

    /**
     * @param string $symbol
     * @return bool
     */
    private function isParenthesis(string $symbol): bool
    {
        return in_array($symbol, self::PARENTHESES, true);
    }

    /**
     * @param string $symbol
     * @return bool
     */
    private function isOpenParenthesis(string $symbol): bool
    {
        return in_array($symbol, self::OPEN_PARENTHESES, true);
    }

    /**
     * @param string $symbol
     * @param string|null $lastOpenParenthesis
     * @return bool
     */
    private function hasExtraCloseParenthesis(string $symbol, ?string $lastOpenParenthesis): bool
    {
        return $symbol && !$lastOpenParenthesis;
    }

    /**
     * @param array $parentheses
     * @return bool
     */
    private function hasExtraOpenParenthesis(array $parentheses): bool
    {
        return !empty($parentheses);
    }

    /**
     * @param array $array
     * @param int $int
     * @return array<int,int>
     */
    public function multiplyArrayValues(array &$array, int $int): array
    {
        foreach ($array as &$item) {
            if (is_array($item)) {
                $this->multiplyArrayValues($item, $int);
            } else {
                $item *= $int;
            }
        }

        // Another solution with build-in php function
//        array_walk_recursive($array, static function (&$item) use ($int) {
//             $item *= $int;
//        });

        return $array;
    }

    /**
     * @param array $array
     * @param int $int
     * @return array<int,int>
     */
    public function getNumbersForCorrectSum(array $array, int $int): array
    {
        $result = [];

        for ($i = 0, $iMax = count($array); $i < $iMax; $i++) {
            for ($k = 1, $kMax = $iMax; $k < $kMax; $k++) {
                if ($array[$i] + $array[$k] === $int && $array[$i] !== $array[$k]) {
                    array_push($result, $array[$i], $array[$k]);
                }
            }
        }

        return array_unique($result);
    }
}
