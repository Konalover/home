<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\MainService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    public function __construct(private readonly MainService $mainService)
    {
    }

    #[Route('/', name: 'app_main')]
    public function index(): Response
    {
        $string = "mary (had a [little] lamb)";
        $firstTestResult = $this->mainService->isCorrectParenthesis($string);
        $firstTestResult = $firstTestResult ? 'Parentheses are correct' : 'Error in parentheses';

        $array = [1, 2, [10, [100, 200], 20, 30], 3, [40, 50]];
        $originalArray = $array;
        $value = 2;
        $secondTestResult = $this->mainService->multiplyArrayValues($array, $value);

        $array3 = [-3, 0, 2, 4, 5];
        $int = 7;
        $numbersForCorrectSum = $this->mainService->getNumbersForCorrectSum($array3, $int);

        return $this->render('main/index.html.twig', [
            'string_with_parentheses' => $string,
            'original_array' => $originalArray,
            'value' => $value,
            'parentheses_test_result' => $firstTestResult,
            'multiply_array_values_test_result' => $secondTestResult,
            'array3' => $array3,
            'int' => $int,
            'numbers_for_correct_sum_test_result' => $numbersForCorrectSum,
        ]);
    }
}
