## Project run
Run containers in docker-compose.yml file. Use url http://localhost:8080/

File src/Controller/MainController.php contains input data.

File src/Service/MainService.php contains logic.

File tests/Service/MainServiceTest.php contains tests.

## Ex. 1 - check if all parentheses have correct pair
Method isCorrectParenthesis() accept string as argument. There we convert string to an array, using foreach we're leaving only parentheses, pushing to array $stock only open parentheses. If the next symbol in iteration is close parenthesis, we're taking last parenthesis from $stock and checking them if they match each other. If not - return false. Also, there is validation if there is more open parentheses than close and vise versa.

## Ex. 2 - multiply nested arrays by value
Method multiplyArrayValues() accept array and integer. There is 2 options: 
1) Using recursive function.
2) Using build-in php method array_walk_recursive(). It iterates all over array values and multiply them by an integer.

## Ex. 3 - search through array values where sum will be equal to value

## Php Unit test
To run tests create CLI Interpreter in Settings->PHP based on php image from docker-compose.yml file
